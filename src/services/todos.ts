import { IToDosResponsePayload } from '../api/todos/response';
import { ITask } from '../store/todos/todos';

export const processToDoEntry = (entry: IToDosResponsePayload): ITask => {
  return {
    task: entry.task,
    isCompleted: Boolean(entry.is_completed),
    isImportant: Boolean(entry.priority),
    id: entry.task_id,
    dueDate: entry.due_date,
    createdAt: entry?.created_at || new Date().toISOString(),
  };
};
