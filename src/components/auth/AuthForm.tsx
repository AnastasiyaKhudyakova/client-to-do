import { observer } from 'mobx-react';
import React from 'react';
import Input from 'react-toolbox/lib/input';
import { useStores } from '../../store/store';
import Button from '@material-ui/core/Button';

const SignUpForm: React.FC = observer(
  (): React.ReactElement => {
    const { authForm, userState } = useStores();
    const {
      formData,
      formErrors,
      isFormValid,
      isLoginForm,
      setError,
      setForm,
    } = authForm;

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      userState.authenticateUser(authForm.formData, !isLoginForm.get());
    };

    return (
      <>
        <Button onClick={() => setForm()} disabled={userState.auth.isLoading}>
          {isLoginForm.get() ? 'Sign up' : 'Log in'}
        </Button>
        <form onSubmit={handleSubmit}>
          {!isLoginForm.get() && (
            <>
              <Input
                type='text'
                label='Name'
                name='name'
                value={formData.name}
                required
                onChange={(value: string) => (formData.name = value)}
                onKeyUp={() => setError('name')}
              />
              <p>{formErrors.get('name')}</p>
            </>
          )}
          <br />
          <Input
            type='email'
            label='Email'
            name='email'
            value={formData.email}
            required
            onChange={(value: string) => (formData.email = value)}
            onKeyUp={() => setError('email')}
          />
          <p>{formErrors.get('email')}</p>
          <br />

          <Input
            type='password'
            label='Password'
            name='password'
            value={formData.password}
            required
            onChange={(value: string) => (formData.password = value)}
            onKeyUp={() => setError('password')}
          />
          <p>{formErrors.get('password')}</p>

          <br />

          <Button type='submit' disabled={!isFormValid.get()}>
            Submit
          </Button>
        </form>
      </>
    );
  },
);

export default SignUpForm;
