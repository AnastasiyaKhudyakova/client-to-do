import { observer } from 'mobx-react';
import React, { useEffect } from 'react';
import { useStores } from '../../store/store';
import { ITask } from '../../store/todos/todos';
import ToDoForm from './ToDoForm';
import ToDo from './ToDo';
import ToDoSortPane from './ToDoSortPane';

const ToDoList: React.FC = observer(
  (): React.ReactElement => {
    const { tasks, userState } = useStores();

    useEffect(() => {
      if (userState.auth.userToken) {
        tasks.fetchToDos(userState.auth.userToken);
      }
    }, [
      tasks.querySettings.offset,
      tasks.querySettings.order_by,
      tasks.querySettings.order,
    ]);

    const todos = tasks.todos.map((entry: ITask) => (
      <ToDo taskData={entry} key={entry.task + entry.createdAt} />
    ));

    return (
      <>
        <ToDoSortPane />
        <ToDoForm />
        <div>{todos.length ? todos : <p>No tasks</p>}</div>;
      </>
    );
  },
);

export default ToDoList;
