import React from 'react';
import { ITask } from '../../store/todos/todos';
import DeleteBtn from './DeleteBtn';

const ToDo: React.FC<{ taskData: ITask }> = ({
  taskData,
}): React.ReactElement => {
  return (
    <div
      style={{
        backgroundColor: taskData.isCompleted ? 'lightgreen' : 'white',
        border: taskData.isImportant ? '1px solid green' : 'none',
      }}
    >
      <h6>{taskData.task}</h6>
      <p>{taskData.createdAt}</p>
      <DeleteBtn id={taskData.id} />
    </div>
  );
};

export default ToDo;
