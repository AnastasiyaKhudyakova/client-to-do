import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import { DatePicker } from '@material-ui/pickers';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { observer } from 'mobx-react';
import React, { FormEvent } from 'react';
import { useStores } from '../../store/store';
import { IToDoPostData } from '../../api/todos/response';

const ToDoForm: React.FC = observer(
  (): React.ReactElement => {
    const { taskForm, userState, tasks } = useStores();
    const { formData } = taskForm;

    const handleSubmit = (e: FormEvent) => {
      e.preventDefault();

      const token = userState.auth.userToken || '';
      const toDoFormData: IToDoPostData = {
        due_date: formData.dueDate || '',
        is_completed: formData.isCompleted,
        priority: formData.isImportant,
        task: formData.task,
      };

      taskForm.isLoading.set(true);

      tasks
        .postToDo(token, toDoFormData)
        .then((res) => tasks.addToDo(res.data))
        .catch((error) => taskForm.setError(error));

      taskForm.isLoading.set(false);
    };

    return (
      <>
        <h5>Add task form</h5>
        <form onSubmit={handleSubmit}>
          <InputLabel>Task</InputLabel>
          <TextField
            name='task'
            value={formData.task}
            type='text'
            multiline={true}
            onChange={(event) => (formData.task = event.target.value)}
          />

          <InputLabel>Completed</InputLabel>
          <Checkbox
            checked={formData.isCompleted}
            value={formData.isCompleted}
            onChange={(event) =>
              taskForm.changeTaskStatus('isCompleted', event.target.checked)
            }
          />

          <InputLabel>Important</InputLabel>
          <Checkbox
            checked={formData.isImportant}
            value={formData.isImportant}
            onChange={(event) =>
              taskForm.changeTaskStatus('isImportant', event.target.checked)
            }
          />

          <InputLabel>Due Date</InputLabel>

          <DatePicker
            value={new Date().toString()}
            onChange={(date: MaterialUiPickersDate) => taskForm.setDate(date)}
          />

          <Button
            variant='contained'
            color='secondary'
            type='submit'
            disabled={!taskForm.isFormValid.get() || taskForm.isLoading.get()}
          >
            Submit
          </Button>
        </form>
        <p>{taskForm.error.get()}</p>
      </>
    );
  },
);

export default ToDoForm;
