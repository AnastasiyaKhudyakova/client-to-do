import React from 'react';
import { Button } from '@material-ui/core';
import { observer } from 'mobx-react';
import { useStores } from '../../store/store';

const DeleteBtn: React.FC<{ id: number }> = observer(
  ({ id }): React.ReactElement => {
    const { tasks, userState } = useStores();
    const handleClick = () => {
      console.log(id);

      const token = userState.auth.userToken || '';

      tasks.deleteToDo(token, id);
    };

    return <Button onClick={handleClick}>Delete</Button>;
  },
);

export default DeleteBtn;
