import { observer } from 'mobx-react';
import React, { useEffect, useRef } from 'react';
import { useStores } from '../../store/store';
import ToDoList from './ToDoList';

const ToDoListContainer: React.FC = observer(() => {
  const { tasks } = useStores();
  const scrollableDiv = useRef(null);

  useEffect(() => {
    const element: any = scrollableDiv.current;

    element?.addEventListener('scroll', (e: any) => {
      if (isScrollBottom(e)) {
        tasks.loadMore.set(true);
      }
    });

    return () => element?.removeEventListener('scroll', isScrollBottom, true);
  }, []);

  const isScrollBottom = (event: any): boolean => {
    const { clientHeight, scrollHeight, scrollTop } = event.target;

    return Math.floor(clientHeight - (scrollHeight - scrollTop)) === 0;
  };

  return (
    <>
      <div
        ref={scrollableDiv}
        id='scrollable'
        style={{
          margin: '0 auto',
          width: '800px',
          height: '1200px',
          overflow: 'auto',
        }}
      >
        <ToDoList />
      </div>
      {tasks.isLoading.get() && <p>Loading ...</p>}
    </>
  );
});

export default ToDoListContainer;
