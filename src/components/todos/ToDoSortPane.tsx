import Fab from '@material-ui/core/Fab/Fab';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';
import Radio from '@material-ui/core/Radio/Radio';
import RadioGroup from '@material-ui/core/RadioGroup/RadioGroup';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import React from 'react';
import { useStores } from '../../store/store';
import { SortDirection, SortFields } from '../../store/todos/todos';

const ToDoSortPane: React.FC = (): React.ReactElement | null => {
  const { tasks } = useStores();

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    value: string,
  ) => tasks.setSortField(value as SortFields);

  const radioBtns = Object.values(SortFields).map((field) => {
    return (
      <FormControlLabel
        key={field}
        value={field}
        control={<Radio />}
        label={field}
      />
    );
  });

  return (
    <>
      <RadioGroup
        aria-label='Sort by'
        name='sortDirection'
        value={tasks.sortField.get()}
        onChange={handleChange}
      >
        {radioBtns}
      </RadioGroup>
      <Fab variant='extended' onClick={() => tasks.toggleSortDirection()}>
        {tasks.querySettings.order === SortDirection.ASC ? (
          <ArrowDownwardIcon />
        ) : (
          <ArrowUpwardIcon />
        )}
      </Fab>
    </>
  );
};

export default ToDoSortPane;
