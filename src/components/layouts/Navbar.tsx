import React from 'react';
import { Link } from 'react-router-dom';

const Navabar: React.FC = (): React.ReactElement => {
  const linkNames: string[] = ['home', 'login', 'todos'];
  const links = linkNames.map((name) => (
    <Link key={`link-to-${name}`} to={`/${name}`}>
      {name}
    </Link>
  ));

  return <ul>{links}</ul>;
};

export default Navabar;
