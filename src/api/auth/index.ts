import { IUserFormData } from '../../store/auth/authForm';

export const getAuthRequestConfig = (userData: IUserFormData) => {
  return {
    method: 'POST',
    body: JSON.stringify(userData),
    headers: {
      'content-type': 'application/json',
    },
  };
};
