export interface ILoginSuccessResPayload {
  message: string;
  userData: ILoginUser;
}

export interface ILoginUser {
  name: string;
  email: string;
  id: number;
  expiresIn: string;
  idToken: string;
}

export interface IUserTokenPayload {
  email: string;
  expiresIn: string;
  iat: number;
  id: number;
  name: string;
}
