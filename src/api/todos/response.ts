export interface ITodosBodyRes {
  status: number;
  tasks: IToDosResponsePayload[];
}

export interface IToDosResponsePayload {
  due_date: string;
  is_completed: number;
  priority: number;
  task: string;
  task_id: number;
  user_id: number;
  created_at?: string;
}

export interface IToDoPostData {
  is_completed: boolean;
  priority: boolean;
  task: string;
  due_date?: string;
}
