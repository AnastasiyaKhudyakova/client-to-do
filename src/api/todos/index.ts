export const getTodosRequestConfig = (
  token: string,
  method: string = 'GET',
  body?: BodyInit | null,
): RequestInit => {
  return {
    method: method,
    headers: {
      Authorization: `Bearer ${token}`,
      'content-type': 'application/json',
    },
    body,
  };
};
