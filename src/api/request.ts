import { BASE_URL } from '../constants/constants';

export const makeRequest = async <T>(path: string, config: RequestInit): Promise<T> => {
  const response = await fetch(`${BASE_URL}/${path}`, config);
  const data = (await response.json()) as T;

  if (response.status >= 400) {
    const errorMessage = (data as any).message;

    throw Error(errorMessage);
  }

  return data;
};
