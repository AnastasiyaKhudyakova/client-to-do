import { observer } from 'mobx-react';
import React, { useEffect } from 'react';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';
import './App.scss';
import SignUpForm from './components/auth/AuthForm';
import ToDoListContainer from './components/todos/ToDoListContainer';
import { useStores } from './store/store';
import Navabar from './components/layouts/Navbar';

const App: React.FC = observer(() => {
  const { userState } = useStores();

  useEffect(() => {
    userState.loadUserFromLocalStorage();
  }, []);

  return (
    <Router>
      <div className='App'>
        <Navabar />
        <Switch>
          <Route exact path='/'>
            <Redirect to={{ pathname: '/home' }} />
          </Route>
          <Route path='/home'>
            <h1>Main page</h1>
          </Route>
          <Route path='/login'>
            {!userState.auth.userToken ? (
              <SignUpForm />
            ) : (
              <Redirect to={{ pathname: '/todos' }} />
            )}
          </Route>
          <Route path='/todos'>
            {userState.auth.userToken ? (
              <ToDoListContainer />
            ) : (
              <Redirect to={{ pathname: '/login' }} />
            )}
          </Route>
          <Route path='/404'>
            <p>Not found</p>
          </Route>
          <Route path='*'>
            <Redirect to={{ pathname: '/404' }} />
          </Route>
        </Switch>
      </div>
    </Router>
  );
});

export default App;
