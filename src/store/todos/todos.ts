import { action, flow, IObservableValue, observable, reaction } from 'mobx';
import { makeRequest } from '../../api/request';
import { getTodosRequestConfig } from '../../api/todos';
import {
  ITodosBodyRes,
  IToDosResponsePayload,
  IToDoPostData,
} from '../../api/todos/response';
import { processToDoEntry } from '../../services/todos';

export interface ITask {
  [x: string]: string | number | boolean | undefined;
  task: string;
  isCompleted: boolean;
  isImportant: boolean;
  createdAt: string;
  id: number;
  dueDate?: string;
}

export interface IToDoRequesQueryParams {
  limit: number;
  offset: number;
  order_by: SortFields;
  order: SortDirection;
}

export enum SortFields {
  DEU_DATE = 'due_date',
  DATE_CREATED = 'created_at',
  PRIORITY = 'priority',
  IS_COMPLETED = 'is_completed',
}

export enum SortDirection {
  ASC = 'asc',
  DESC = 'desc',
}

export const createToDoStore = () => {
  const todos: ITask[] = observable.array([]);
  const isLoading: IObservableValue<boolean> = observable.box(false);
  const loadMore: IObservableValue<boolean> = observable.box(false);
  const sortField = observable.box(SortFields.DATE_CREATED);
  const querySettings = observable.object<IToDoRequesQueryParams>({
    limit: 10,
    offset: 0,
    order_by: SortFields.DATE_CREATED,
    order: SortDirection.ASC,
  });

  const setSortField = action((value: SortFields) => {
    sortField.set(value);
    resetStore();
  });

  const toggleSortDirection = action(() => {
    const sortDirection =
      querySettings.order === SortDirection.ASC
        ? SortDirection.DESC
        : SortDirection.ASC;

    querySettings.order = sortDirection;
    resetStore();
  });

  const addToDo = action((todo: IToDosResponsePayload) =>
    todos.push(processToDoEntry(todo)),
  );

  const fetchToDos = flow(function* (token: string) {
    isLoading.set(true);

    try {
      const result: ITodosBodyRes = yield makeRequest(
        `todos?limit=${querySettings.limit}&offset=${querySettings.offset}&order_by=${querySettings.order_by}&order=${querySettings.order}`,
        getTodosRequestConfig(token),
      );

      if (!result.tasks.length) {
        disposer();
      }

      result.tasks.forEach((entry) => {
        const todoIndex = todos.findIndex((todo) => todo.id === entry.task_id);

        if (todoIndex === -1) {
          addToDo(entry);
        }
      });

      loadMore.set(false);
    } catch (error) {
      console.log(error);
    }

    isLoading.set(false);
  });

  const postToDo = flow(function* (token: string, taskFormData: IToDoPostData) {
    const result = yield makeRequest(
      'todos',
      getTodosRequestConfig(token, 'POST', JSON.stringify(taskFormData)),
    );

    return result;
  });

  const deleteToDo = flow(function* (token: string, id: number) {
    try {
      yield makeRequest(`todos/${id}`, getTodosRequestConfig(token, 'DELETE'));

      const index = todos.findIndex((todo: ITask) => todo.id === id);

      if (index > -1) {
        todos.splice(index, 1);
      }
    } catch (error) {
      console.log(error);
    }
  });

  const resetStore = () => {
    todos.length = 0;
    resetQuerySetting();

    disposer = createScrollReaction();
  };

  const createScrollReaction = () => {
    return reaction(
      () => loadMore.get(),
      () => {
        if (loadMore.get()) {
          querySettings.offset = todos.length;
        }
      },
    );
  };

  const resetQuerySetting = () => {
    querySettings.limit = 10;
    querySettings.offset = 0;
    querySettings.order_by = sortField.get();
  };

  let disposer = createScrollReaction();

  return {
    addToDo,
    deleteToDo,
    fetchToDos,
    isLoading,
    loadMore,
    postToDo,
    querySettings,
    setSortField,
    sortField,
    todos,
    toggleSortDirection,
  };
};

export type TaskStore = ReturnType<typeof createToDoStore>;
