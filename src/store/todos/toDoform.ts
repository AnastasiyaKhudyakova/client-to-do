import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { formatISO } from 'date-fns';
import { IObservableValue, observable, reaction } from 'mobx';

export interface IToDoFormData {
  task: string;
  isCompleted: boolean;
  isImportant: boolean;
  dueDate: string;
}

export const MIN_TASK_LENGTH: number = 20;
export const MAX_TASK_LENGTH: number = 200;

export const createToDoFormStore = () => {
  const isFormValid: IObservableValue<boolean> = observable.box(false);
  const isLoading: IObservableValue<boolean> = observable.box(false);
  const error: IObservableValue<string> = observable.box('');

  const formData = observable.object({
    task: '',
    isCompleted: false,
    isImportant: true,
    dueDate: formatISO(new Date()).split('T')[0],
  });

  reaction(
    () => formData.task,
    () => isFormValid.set(isTaskLengthValid()),
  );

  const changeTaskStatus = (fiedlName: string, val: boolean) =>
    ((formData as any)[fiedlName] = val);

  const setDate = (date: MaterialUiPickersDate) => {
    const pickedDate = date || new Date();

    formData.dueDate = formatISO(pickedDate);
  };

  const setError = (err: Error) => error.set(err.message);

  const isTaskLengthValid = () => {
    return (
      formData.task.trim().length >= MIN_TASK_LENGTH &&
      formData.task.trim().length <= MAX_TASK_LENGTH
    );
  };

  return {
    changeTaskStatus,
    error,
    formData,
    isFormValid,
    isLoading,
    setDate,
    setError,
  };
};

export type TaskFormStore = ReturnType<typeof createToDoFormStore>;
