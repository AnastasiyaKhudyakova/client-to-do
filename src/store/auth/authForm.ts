import { action, IObservableValue, observable, reaction, values } from 'mobx';
import { AuthFormFieldKey, authFormValidator } from '../../validators/authForm';

export interface IUserFormData {
  email: string;
  password: string;
  name: string;
}

export const createAuthFormStore = () => {
  const isLoginForm: IObservableValue<boolean> = observable.box(true);
  const isFormValid: IObservableValue<boolean> = observable.box(false);

  const formData = observable.object({
    name: '',
    email: '',
    password: '',
  });

  const formErrors = observable.map({
    name: '',
    email: '',
    password: '',
  });

  const setError = action((fieldsName: AuthFormFieldKey) => {
    if (formErrors.has(fieldsName)) {
      formErrors.set(
        fieldsName,
        authFormValidator(fieldsName, formData[fieldsName]),
      );
    }
  });

  const setForm = () => {
    isLoginForm.set(!isLoginForm.get());
    resetForm();
  };

  const resetForm = () => {
    Object.keys(formData).forEach(
      (field: string) => ((formData as any)[field] = ''),
    );
    Object.keys(formErrors).forEach((field: string) =>
      formErrors.set(field, ''),
    );

    isFormValid.set(false);
  };

  reaction(
    () => values(formErrors),
    (errors) => isFormValid.set(errors.every((field) => !field)),
  );

  return {
    formData,
    formErrors,
    setError,
    isFormValid,
    isLoginForm,
    setForm,
  };
};

export type AuthFormStore = ReturnType<typeof createAuthFormStore>;
