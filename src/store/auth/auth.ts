import { flow, observable } from 'mobx';
import { getAuthRequestConfig } from '../../api/auth';
import { ILoginSuccessResPayload } from '../../api/auth/response';
import { makeRequest } from '../../api/request';
import { LOCAL_STORAGE_USER } from '../../constants/constants';
import { IUserFormData } from './authForm';
import { isValidToken } from '../../utils/jwtDecoder';

export type UserToken = string | null;

export interface IUserState {
  userToken: UserToken | null;
  isLoading: boolean;
  error: string | null;
}

export const createUserStore = () => {
  const auth: IUserState = observable.object({
    userToken: null,
    isLoading: false,
    error: null,
  });

  const authenticateUser = flow(function* (
    userFormData: IUserFormData,
    isSignUp?: boolean,
  ) {
    auth.isLoading = true;

    const pathName = isSignUp ? 'signUp' : 'signIn';

    try {
      const result: ILoginSuccessResPayload = yield makeRequest(
        `auth/${pathName}`,
        getAuthRequestConfig(userFormData),
      );

      auth.userToken = result?.userData?.idToken;

      localStorage.setItem(LOCAL_STORAGE_USER, JSON.stringify(auth.userToken));
    } catch (error) {
      auth.error = error.message;
    }
    auth.isLoading = false;
  });

  const loadUserFromLocalStorage = () => {
    const loginUserData = localStorage.getItem(LOCAL_STORAGE_USER);

    if (loginUserData) {
      const loginUser = JSON.parse(loginUserData);

      if (isValidToken(loginUser)) {
        auth.userToken = loginUser;
      }
    }
  };

  const logout = () => {
    localStorage.removeItem(LOCAL_STORAGE_USER);
    auth.userToken = null;
  };

  return {
    auth,
    authenticateUser,
    loadUserFromLocalStorage,
    logout,
  };
};

export type UserStore = ReturnType<typeof createUserStore>;
