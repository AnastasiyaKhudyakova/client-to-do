import React from 'react';
import { createUserStore, UserStore } from './auth/auth';
import { AuthFormStore, createAuthFormStore } from './auth/authForm';
import { createToDoStore, TaskStore } from './todos/todos';
import { TaskFormStore, createToDoFormStore } from './todos/toDoform';

export interface IAppStore {
  tasks: TaskStore;
  taskForm: TaskFormStore;
  authForm: AuthFormStore;
  userState: UserStore;
}

export const storesContext = React.createContext({
  tasks: createToDoStore(),
  taskForm: createToDoFormStore(),
  authForm: createAuthFormStore(),
  userState: createUserStore(),
});

export const useStores = () => React.useContext(storesContext);
