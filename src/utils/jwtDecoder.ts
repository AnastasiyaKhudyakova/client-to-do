import jwt_decode from 'jwt-decode';
import { IUserTokenPayload } from '../api/auth/response';
// ToDo handle case when jwt_decode throws error

const isValidToken = (token: string): boolean => {
  const decoded = jwt_decode(token) as IUserTokenPayload;
  const { iat, expiresIn } = decoded;
  const validTokenPeriod = parseInt(expiresIn);

  console.log(validTokenPeriod);

  if (!Number.isNaN(validTokenPeriod)) {
    return (iat + validTokenPeriod * 3600) * 1000 > new Date().getTime();
  }

  return false;
};

export { isValidToken };
