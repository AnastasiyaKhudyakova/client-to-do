/* eslint-disable no-control-regex */
export const EMAIL_REGEX = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

export enum AuthFormFields {
  'name',
  'email',
  'password',
}

export type AuthFormFieldKey = keyof typeof AuthFormFields;

export const authFormValidator = (
  fieldName: AuthFormFieldKey,
  fieldValue: string,
) => {
  let error: string = '';

  switch (true) {
    case fieldValue.trim().length < 1:
      error = `${fieldName} is required`;
      break;
    case fieldName === 'name' && fieldValue.length < 3:
      error = "Name musn't be shotter than 3 characters";
      break;
    case fieldName === 'name' && fieldValue.length > 20:
      error = "Name musn't be longer than 20 characters";
      break;
    case fieldName === 'email':
      if (!EMAIL_REGEX.test(fieldValue)) {
        error = 'Invalid email';
      }
      break;
    case fieldName === 'password': {
      if (!/^(?!\s+)(?=.*[A-Z]+)[A-Za-z0-9]{8,}$/.test(fieldValue)) {
        error = 'Weak password';
      }
      break;
    }
    default:
      error = '';
  }

  return error;
};
